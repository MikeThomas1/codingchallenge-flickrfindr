# Flickr Findr

This app uses the [Flickr API][FLKRAPI] to show popular photos and search for photos. The full project spec can be seen [here][SPEC].

## Getting started

1. Open `FlickrFindr.xcodeproj`
1. Click run


## Improvement TODOs

1. Write tests around `SearchPhotoListViewModel.swift`
1. Abstract pagination logic from `SearchPhotoListViewModel.swift`
1. Add no results label for search terms that return no results
1. Add loading UI for full sized image (details)


[FLKRAPI]: https://www.flickr.com/services/api/
[SPEC]: Docs/Spec.pdf
