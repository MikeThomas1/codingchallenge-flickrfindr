//
//  AppDelegate.swift
//  FlickrFindr
//
//  Created by Michael Thomas on 3/30/18.
//  Copyright © 2018 Michael Thomas. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder {
    var window: UIWindow?
}

extension AppDelegate: UIApplicationDelegate { }

