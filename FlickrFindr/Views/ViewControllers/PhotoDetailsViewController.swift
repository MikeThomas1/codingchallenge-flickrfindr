//
//  PhotoDetailsViewController.swift
//  FlickrFindr
//
//  Created by Michael Thomas on 4/1/18.
//  Copyright © 2018 Michael Thomas. All rights reserved.
//

import UIKit


class PhotoDetailsViewController: UIViewController {
    var viewModel: PhotoDetailsViewModel! {
        didSet {
            viewModel.delegate = self
        }
    }
    
    @IBOutlet private weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateUI(with: viewModel.details)
        
        viewModel.fetchImage { [weak self] (error) in
            guard error != nil else {
                return
            }
            self?.presentError(message: "Could not fetch image")
        }
    }
}

extension PhotoDetailsViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 6.0
        return imageView
    }
}

extension PhotoDetailsViewController: PhotoDetailsViewModelDelegate {
    func photoDetailsVM(_ vm: PhotoDetailsViewModel, didUpdateDetails details: PhotoDetailsViewModel.Details) {
        DispatchQueue.main.async {
            self.updateUI(with: details)
        }
    }
}

private extension PhotoDetailsViewController {
    func updateUI(with details: PhotoDetailsViewModel.Details) {
        title = details.title
        imageView.image = details.image
    }
}
