//
//  PhtotoSearchCoordinatorViewController.swift
//  FlickrFindr
//
//  Created by Michael Thomas on 3/31/18.
//  Copyright © 2018 Michael Thomas. All rights reserved.
//

import UIKit


class PhtotoSearchCoordinatorViewController: UITableViewController {
    weak var photoSearchVC: SearchPhotoListViewController?
    
    private lazy var debouncedSearch = Debouncer(delay: 0.8) { [weak self] in
        if let searchTerm = self?.navigationItem.searchController?.searchBar.text, searchTerm.count > 0 {
            self?.photoSearchVC?.search(searchTerm)
            self?.searchHistoryViewModel.updateSearchHistory(searchTerm: searchTerm)
        } else {
            self?.photoSearchVC?.clearResults()
        }
    }
    private let searchHistoryViewModel = PhotoSearchHistoryViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchHistoryViewModel.delegate = self
        prepareSearchController()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vm = (sender as? PhotoDetailsViewModel),
            segue.identifier == "showPhotoDetails",
            let vc = segue.destination as? PhotoDetailsViewController {
            vc.viewModel = vm
        }
    }
    
    // MARK: - UITableViewDataSource
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchHistoryCell", for: indexPath)
        let row = searchHistoryViewModel.rowAt(indexPath)
        cell.textLabel?.text = row
        return cell
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return searchHistoryViewModel.numberOfSections()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchHistoryViewModel.numberOfRowsInSection(section)
    }
    
    // MARK: - UITableViewDelegate
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return searchHistoryViewModel.titleForHeaderFooterInSection(section)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        defer { tableView.deselectRow(at: indexPath, animated: true) }
        
        let searchTerm = searchHistoryViewModel.rowAt(indexPath)
        navigationItem.searchController?.isActive = true
        navigationItem.searchController?.searchBar.text = searchTerm
    }
}

private extension PhtotoSearchCoordinatorViewController {
    func prepareSearchController() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        photoSearchVC = mainStoryboard.instantiateViewController(withIdentifier: "PhotoListViewController") as? SearchPhotoListViewController
        photoSearchVC?.delegate = self
        
        let searchController = UISearchController(searchResultsController: photoSearchVC)
        searchController.searchResultsUpdater = self
        searchController.searchBar.tintColor = .white
        searchController.searchBar.barStyle = .black
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.delegate = self
        
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        definesPresentationContext = true
    }
}

extension PhtotoSearchCoordinatorViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        debouncedSearch.call()
    }
}

extension PhtotoSearchCoordinatorViewController: UISearchControllerDelegate {
    func didDismissSearchController(_ searchController: UISearchController) {
        photoSearchVC?.clearResults()
    }
}

extension PhtotoSearchCoordinatorViewController: PhotoSearchHistoryViewModelDelgate {
    func PhotoSearchHistoryVM(_ vm: PhotoSearchHistoryViewModel, didUpdateSections section: [PhotoSearchHistoryViewModel.SearchHistorySection]) {
        tableView.reloadData()
    }
}

extension PhtotoSearchCoordinatorViewController: SearchPhotoListViewControllerDelegate {
    func searchPhotoListVC(_ vc: SearchPhotoListViewController, shouldDisplayDetails detailsVM: PhotoDetailsViewModel) {
        navigationItem.searchController?.searchBar.resignFirstResponder()
        performSegue(withIdentifier: "showPhotoDetails", sender: detailsVM)
    }
}
