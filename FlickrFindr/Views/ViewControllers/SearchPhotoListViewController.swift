//
//  SearchPhotoListViewController.swift
//  FlickrFindr
//
//  Created by Michael Thomas on 3/30/18.
//  Copyright © 2018 Michael Thomas. All rights reserved.
//

import UIKit


protocol SearchPhotoListViewControllerDelegate: class {
    func searchPhotoListVC(_ vc: SearchPhotoListViewController, shouldDisplayDetails detailsVM: PhotoDetailsViewModel)
}

class SearchPhotoListViewController: UITableViewController {
    private let viewModel = SearchPhotoListViewModel()
    weak var delegate: SearchPhotoListViewControllerDelegate?
    
    private let fetchQueue = DispatchQueue(label: "search.fetch.queue", qos: .userInitiated)
    private lazy var debounceReload = Debouncer(delay: 0.2) { [weak self] in
        self?.tableView.reloadData()
    }
    
    func search(_ searchTerm: String) {
        viewModel.beginFetchingPages(matching: searchTerm)
    }
    
    func clearResults() {
        viewModel.clearResults()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.delegate = self
        tableView.prefetchDataSource = self
    }
    
    // MARK: - UITableViewDataSource
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PhotoCell", for: indexPath) as? PhotoCell else {
            fatalError("Unexpected Row Type")
        }
        
        let row = viewModel.rowAt(indexPath) ?? SearchPhotoListViewModel.Row(id: nil, title: nil)
        cell.configure(with: row)
        
        return cell
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsInSection(section)
    }
    
    // MARK: - UITableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        defer { tableView.deselectRow(at: indexPath, animated: true) }
        
        guard let detailsVM = viewModel.detailsViewModel(for: indexPath) else {
            presentError(message: "Could not load details")
            return
        }
        delegate?.searchPhotoListVC(self, shouldDisplayDetails: detailsVM)
    }
}

extension SearchPhotoListViewController: UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        fetchQueue.async { [weak self] in
            for indexPath in indexPaths {
                self?.viewModel.requestNextPageIfNeeded(for: indexPath)
            }
        }
    }
}

extension SearchPhotoListViewController: SearchPhotoListViewModelDelegate {
    func searchPhotoListVM(_ vm: SearchPhotoListViewModel, didEncounterError error: Error) {
        DispatchQueue.main.async {
            self.presentError(message: "Could not fetch photo")
        }
    }
    
    func searchPhotoListVMDidReloadData(_ vm: SearchPhotoListViewModel) {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func searchPhotoListVM(_ vm: SearchPhotoListViewModel, didUpdateRowsAt indexPaths: [IndexPath]) {
        DispatchQueue.main.async {
            // calling self.tableView.reloadRows(at: indexPaths, with: .none)
            // everytime a new picture is loaded makes for a huge performance hit.
            // For this reason we debounce reloads.
            self.debounceReload.call()
        }
    }
}
