//
//  PhotoCell.swift
//  FlickrFindr
//
//  Created by Michael Thomas on 3/31/18.
//  Copyright © 2018 Michael Thomas. All rights reserved.
//

import UIKit


class PhotoCell: UITableViewCell {
    @IBOutlet private weak var loadingView: UIView?
    @IBOutlet private weak var photoImageView: UIImageView?
    @IBOutlet private weak var photoTitleLabel: UILabel?
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    func configure(with photoRow: SearchPhotoListViewModel.Row) {
        let hasImage = (photoRow.image != nil)
        let hasTitle = (photoRow.title != nil)
        let hasId = (photoRow.id != nil)
        
        isUserInteractionEnabled = hasId
        loadingView?.isHidden = hasImage
        hasImage ? activityIndicator.stopAnimating() : activityIndicator.startAnimating()
        photoImageView?.isHidden = !hasImage
        photoImageView?.image = photoRow.image
        photoTitleLabel?.textColor = hasTitle ? .black : .lightGray
        photoTitleLabel?.text = photoRow.title ?? "████████████"
    }
}
