//
//  PhotoPageInfo.swift
//  FlickrFindr
//
//  Created by Michael Thomas on 3/31/18.
//  Copyright © 2018 Michael Thomas. All rights reserved.
//

import Foundation


struct PhotoPageInfo: Codable {
    let page: Int
    let pages: Int
    let perPage: Int
    let total: String
    let photos: [Photo]
    
    enum CodingKeys: String, CodingKey {
        case page
        case pages
        case perPage = "perpage"
        case total
        case photos = "photo"
    }
}
