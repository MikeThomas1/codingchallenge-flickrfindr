//
//  PhotosPage.swift
//  FlickrFindr
//
//  Created by Michael Thomas on 3/31/18.
//  Copyright © 2018 Michael Thomas. All rights reserved.
//

import Foundation

struct PhotosContainer: Codable {
    let pageInfo: PhotoPageInfo
    let stat: String
    
    enum CodingKeys: String, CodingKey {
        case pageInfo = "photos"
        case stat
    }
}


