//
//  FlickrClient.swift
//  FlickrFindr
//
//  Created by Michael Thomas on 3/31/18.
//  Copyright © 2018 Michael Thomas. All rights reserved.
//

import Foundation


protocol FlickrClient {
    func fetchPhotos(matching searchTerm: String, page: Int, perPage: Int, completion: @escaping ResultCompletion<PhotosContainer>)
    func fetchPhoto(_ photo: Photo, quality: PhotoQuality?, completion: @escaping ResultCompletion<Data>)
}

enum PhotoQuality: String {
    case s    // small square 75x75
    case q    // large square 150x150
    case t    // thumbnail, 100 on longest side
    case m    // small, 240 on longest side
    case n    // small, 320 on longest side
    case z    // medium 640, 640 on longest side
    case c    // medium 800, 800 on longest side†
    case b    // large, 1024 on longest side*
    case h    // large 1600, 1600 on longest side†
    case k    // large 2048, 2048 on longest side†
    case o    // original image, either a jpg, gif or png, depending on source format
}

enum FlickrClientError: Error {
    case sessionRequestError(Error)
    case badResponseCode(Int)
    case emptyResponseData
    case parsingError(Error)
    case malformedURL
}
