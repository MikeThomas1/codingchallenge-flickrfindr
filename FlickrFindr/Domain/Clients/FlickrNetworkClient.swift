//
//  FlickrNetworkClient.swift
//  FlickrFindr
//
//  Created by Michael Thomas on 3/31/18.
//  Copyright © 2018 Michael Thomas. All rights reserved.
//

import Foundation


class FlickrNetworkClient: FlickrClient {
    static let shared = FlickrNetworkClient()
    
    private let session = URLSession(configuration: .ephemeral)
    private let baseURL = URL(string: "https://api.flickr.com/services/rest/")!
    private let apiKey = "1508443e49213ff84d566777dc211f2a"
    
    // Only shared instance should be used
    private init() { }
    
    func fetchPhotos(matching searchTerm: String, page: Int = 1, perPage: Int = 25, completion: @escaping ResultCompletion<PhotosContainer>) {
        let parameters = ["method": "flickr.photos.search", "text": searchTerm, "per_page": "\(perPage)", "page": "\(page)", "sort": "relevance"]
        performGetRequest(parameters: parameters, completion: completion)
    }
    
    func fetchPhoto(_ photo: Photo, quality: PhotoQuality? = nil, completion: @escaping ResultCompletion<Data>) {
        // https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{o-secret}_o.(jpg|gif|png)
        var urlString = "https://farm\(photo.farm).staticflickr.com/\(photo.server)/\(photo.id)_\(photo.secret)"
        if let quality = quality {
            urlString = urlString + "_\(quality.rawValue)"
        }
        urlString = urlString + ".jpg"
        
        guard let url = URL(string: urlString) else {
            completion(.failure(FlickrClientError.malformedURL))
            return
        }
        let request = URLRequest(url: url)
        performRequest(request, completion: completion)
    }
}

private extension FlickrNetworkClient {
    func performGetRequest<ModelType: Codable>(_ path: String = "", parameters: [String: String], completion: @escaping ResultCompletion<ModelType>) {
        // Add the base parameters to the parameter list. These are sensible to have in every get request.
        var params = parameters
        params["nojsoncallback"] = "1"
        params["format"] = "json"
        params["api_key"] = apiKey
        
        let queryItems = params.map(URLQueryItem.init(name:value:))
        var urlComponents = URLComponents(url: baseURL, resolvingAgainstBaseURL: true)
        let basePath = urlComponents?.path ?? ""
        urlComponents?.path = basePath + path
        urlComponents?.queryItems = queryItems
        guard let url = urlComponents?.url else {
            completion(.failure(FlickrClientError.malformedURL))
            return
        }
        
        let urlRequest = URLRequest(url: url)
        performRequest(urlRequest, completion: completion)
    }
    
    func performRequest<ModelType: Codable>(_ urlRequest: URLRequest, completion: @escaping ResultCompletion<ModelType>) {
        performRequest(urlRequest) { (result) in
            switch result {
            case .success(let data):
                // TODO: Use `keyDecodingStrategy` so I can remove all
                // of my CodingKeys in the model classes
                let decoder = JSONDecoder()
                do {
                    let model = try decoder.decode(ModelType.self, from: data)
                    completion(.success(model))
                    return
                } catch let parseError {
                    let parsingError: FlickrClientError = .parsingError(parseError)
                    completion(.failure(parsingError))
                    return
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func performRequest(_ urlRequest: URLRequest, completion: @escaping ResultCompletion<Data>) {
        let dataTask = session.dataTask(with: urlRequest) { (data, urlResponse, error) in
            // Guard against error
            if let error = error {
                let sessionError: FlickrClientError = .sessionRequestError(error)
                completion(.failure(sessionError))
                return
            }
            
            if let httpURLResponse = urlResponse as? HTTPURLResponse,
                !(200...300).contains(httpURLResponse.statusCode) {
                let responseError: FlickrClientError = .badResponseCode(httpURLResponse.statusCode)
                completion(.failure(responseError))
                return
            }
            
            guard let data = data else {
                let emptyDataError: FlickrClientError = .emptyResponseData
                completion(.failure(emptyDataError))
                return
            }
            completion(.success(data))
        }
        dataTask.resume()
    }

}
