//
//  PhotoSearchHistoryViewModel.swift
//  FlickrFindr
//
//  Created by Michael Thomas on 4/1/18.
//  Copyright © 2018 Michael Thomas. All rights reserved.
//

import Foundation


protocol PhotoSearchHistoryViewModelDelgate: class {
    func PhotoSearchHistoryVM(_ vm: PhotoSearchHistoryViewModel, didUpdateSections section: [PhotoSearchHistoryViewModel.SearchHistorySection])
}

class PhotoSearchHistoryViewModel: TableViewModel {
    typealias SearchHistorySection = Section<String, String>
    var sections = [SearchHistorySection]() {
        didSet {
            delegate?.PhotoSearchHistoryVM(self, didUpdateSections: sections)
        }
    }
    weak var delegate: PhotoSearchHistoryViewModelDelgate?
    
    private let persistenceManager = UserDefaultsPersistenceManager()
    
    init() {
        let searchTerms: [String] = persistenceManager.object(forKey: .searchHistory) ?? []
        updateSections(with: searchTerms)
    }
    
    func updateSearchHistory(searchTerm: String) {
        let searchTerms = persistenceManager.insertSetItem(searchTerm, forKey: .searchHistory) ?? Set<String>()
        updateSections(with: Array(searchTerms))
    }
}


private extension PhotoSearchHistoryViewModel {
    func updateSections(with searchTerms: [String]) {
        self.sections = [SearchHistorySection(sectionInfo: "Search History", rows: searchTerms)]
    }
}
