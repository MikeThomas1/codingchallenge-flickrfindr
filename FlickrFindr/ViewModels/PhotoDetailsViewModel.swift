//
//  PhotoDetailsViewModel.swift
//  FlickrFindr
//
//  Created by Michael Thomas on 4/1/18.
//  Copyright © 2018 Michael Thomas. All rights reserved.
//

import UIKit


protocol PhotoDetailsViewModelDelegate: class {
    func photoDetailsVM(_ vm: PhotoDetailsViewModel, didUpdateDetails details: PhotoDetailsViewModel.Details)
}

class PhotoDetailsViewModel {
    weak var delegate: PhotoDetailsViewModelDelegate?
    
    private let client: FlickrClient
    private let photo: Photo
    var details: Details {
        didSet {
            delegate?.photoDetailsVM(self, didUpdateDetails: details)
        }
    }
    
    init(_ client: FlickrClient = FlickrNetworkClient.shared, photo: Photo) {
        self.client = client
        self.photo = photo
        self.details = Details(title: photo.title, image: nil)
    }
    
    func fetchImage(completion: @escaping (Error?) -> Void) {
        client.fetchPhoto(photo, quality: nil) { (result) in
            switch result {
            case .success(let imageData):
                let image = UIImage(data: imageData)
                self.details = Details(title: self.photo.title, image: image)
                completion(nil)
            case .failure(let error):
                completion(error)
                return
            }
        }
    }
    
    struct Details {
        let title: String
        let image: UIImage?
    }
}
