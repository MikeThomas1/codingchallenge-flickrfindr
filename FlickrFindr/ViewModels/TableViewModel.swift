//
//  TableViewModel.swift
//  FlickrFindr
//
//  Created by Michael Thomas on 3/31/18.
//  Copyright © 2018 Michael Thomas. All rights reserved.
//

import UIKit


protocol TableViewModel {
    associatedtype Row
    associatedtype SectionInfo
    var sections: [Section<SectionInfo, Row>] { get set }
    
    func rowAt(_ indexPath: IndexPath) -> Row?
    func numberOfSections() -> Int
    func numberOfRowsInSection(_ section: Int) -> Int
    func titleForHeaderFooterInSection(_ section: Int) -> SectionInfo?
}

// Since some of these methods are the same for most cases, create a default impl for them
extension TableViewModel {
    func numberOfSections() -> Int {
        return sections.count
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        return sections[safe: section]?.rows.count ?? 0
    }
    
    func rowAt(_ indexPath: IndexPath) -> Row? {
        return sections[safe: indexPath.section]?.rows[safe: indexPath.row]
    }
    
    func titleForHeaderFooterInSection(_ section: Int) -> SectionInfo? {
        return sections[safe: section]?.sectionInfo
    }
}

