//
//  SearchPhotoListViewModel.swift
//  FlickrFindr
//
//  Created by Michael Thomas on 3/31/18.
//  Copyright © 2018 Michael Thomas. All rights reserved.
//

import UIKit


protocol SearchPhotoListViewModelDelegate: class {
    func searchPhotoListVM(_ vm: SearchPhotoListViewModel, didUpdateRowsAt indexPaths: [IndexPath])
    func searchPhotoListVM(_ vm: SearchPhotoListViewModel, didEncounterError error: Error)
    func searchPhotoListVMDidReloadData(_ vm: SearchPhotoListViewModel)
}

class SearchPhotoListViewModel {
    typealias PhotoSection = Section<Any?, Row>
    
    weak var delegate: SearchPhotoListViewModelDelegate?

    var sections = [PhotoSection]()
    
    private let client: FlickrClient
    private var pages = [PhotoPageInfo]()
    private var pageRequestsInFlight = [String: Set<Int>]()
    private var imageRequestsInFlight = [String: Set<String>]()
    private let pageSize = 25
    private var lastRequestedSearchTerm = ""
    // NSCache won't accept String, only NSString
    private static let imageCache = NSCache<NSString, UIImage>()
    
    
    // Primarily for testing
    init(_ client: FlickrClient = FlickrNetworkClient.shared) {
        self.client = client
    }
    
    /**
     Begins fetching fresh pages beginning at page 1
     */
    func beginFetchingPages(matching searchTerm: String) {
        guard lastRequestedSearchTerm != searchTerm else {
            return
        }
        
        reloadWithLoadingRows(total: pageSize)
        
        fetchPhotos(matching: searchTerm, page: 1)
    }
    
    func clearResults() {
        lastRequestedSearchTerm = ""
        reloadWithLoadingRows(total: 0)
    }
    
    func requestNextPageIfNeeded(for indexPath: IndexPath) {
        guard let page = pageNeeded(for: indexPath, searchTerm: lastRequestedSearchTerm) else {
            return
        }
        
        fetchPhotos(matching: lastRequestedSearchTerm, page: page)
    }
    
    func detailsViewModel(for indexPath: IndexPath) -> PhotoDetailsViewModel? {
        let row = rowAt(indexPath)
        guard let photo = pages.flatMap({ $0.photos }).filter({ $0.id == row?.id }).first else {
            return nil
        }
        let vm = PhotoDetailsViewModel(photo: photo)
        return vm
    }
    
    struct Row {
        var id: String?
        var title: String?
        var image: UIImage? {
            get {
                if let id = id {
                    return SearchPhotoListViewModel.imageCache.object(forKey: id as NSString)
                } else {
                    return nil
                }
            }
            set {
                if let image = newValue, let id = id {
                    SearchPhotoListViewModel.imageCache.setObject(image, forKey: id as NSString)
                }
            }
        }
    }
}

extension SearchPhotoListViewModel: TableViewModel { }

private extension SearchPhotoListViewModel {
    func fetchPhotos(matching searchTerm: String, page: Int) {
        print("fetching page: \(page)")
        lastRequestedSearchTerm = searchTerm
        
        var requestsForSearchTerm = pageRequestsInFlight[searchTerm] ?? Set<Int>()
        requestsForSearchTerm.insert(page)
        pageRequestsInFlight[searchTerm] = requestsForSearchTerm
        client.fetchPhotos(matching: searchTerm, page: page, perPage: pageSize) { result in
            self.pageRequestsInFlight[searchTerm]?.remove(page)
            switch result {
            case .success(let photoContainer):
                if page != photoContainer.pageInfo.page {
                    fatalError("The page I sent is not what I got back")
                }
                if self.pages.isEmpty {
                    self.reloadWithLoadingRows(total: Int(photoContainer.pageInfo.total)!)
                }
                if self.lastRequestedSearchTerm == searchTerm {
                    self.pages.append(photoContainer.pageInfo)
                    self.updateRows(pageInfo: photoContainer.pageInfo)
                    self.fetchThumbnails(pageInfo: photoContainer.pageInfo, searchTerm: searchTerm)
                }
            case .failure(let error):
                self.delegate?.searchPhotoListVM(self, didEncounterError: error)
                return
            }
        }
    }
    
    func fetchThumbnails(pageInfo: PhotoPageInfo, searchTerm: String) {
        for (i, photo) in pageInfo.photos.enumerated() {
            guard SearchPhotoListViewModel.imageCache.object(forKey: photo.id as NSString) == nil else {
                continue
            }
            let hasImageRequestInFlight = imageRequestsInFlight[searchTerm]?.contains(photo.id) ?? false
            guard !hasImageRequestInFlight else {
                continue
            }
            let indexPath = derivedIndexPath(for: pageInfo, photoPageIndex: i)
            var requestsForImage = imageRequestsInFlight[searchTerm] ?? Set<String>()
            requestsForImage.insert(photo.id)
            imageRequestsInFlight[searchTerm] = requestsForImage
            client.fetchPhoto(photo, quality: .t) { (result) in
                self.imageRequestsInFlight[searchTerm]?.remove(photo.id)
                guard self.lastRequestedSearchTerm == searchTerm else {
                    return
                }
                switch result {
                case .success(let data):
                    self.updateRow(at: indexPath, with: data)
                    return
                case .failure:
                    // Many of these fail because the thumbnail image is missing on Flickr's server. Don't propegate the error to the UI
                    let image = #imageLiteral(resourceName: "MissingOnServer")
                    self.updateRow(at: indexPath, with: image)
                    print("Couldn't load image: \(photo.title)")
                    return
                }
            }
        }
    }
    
    func reloadWithLoadingRows(total: Int) {
        sections = [PhotoSection]()
        pages = [PhotoPageInfo]()
        
        // Create some loading rows
        var rows = [Row]()
        for _ in 0..<total { rows.append(Row(id: nil, title: nil)) }
        sections.append(Section(sectionInfo: nil, rows: rows))
        
        delegate?.searchPhotoListVMDidReloadData(self)
    }
    
    func updateRows(pageInfo: PhotoPageInfo) {
        var indexPaths = [IndexPath]()
        for (i, photo) in pageInfo.photos.enumerated() {
            let indexPath = derivedIndexPath(for: pageInfo, photoPageIndex: i)
        
            guard var row = rowAt(indexPath), row.title == nil else {
                continue
            }
            
            indexPaths.append(indexPath)
            row.title = photo.title
            row.id = photo.id
            sections[indexPath.section].rows[indexPath.row] = row
        }
        delegate?.searchPhotoListVM(self, didUpdateRowsAt: indexPaths)
    }
    
    func updateRow(at indexPath: IndexPath, with photo: Data) {
        let image = UIImage(data: photo) ?? #imageLiteral(resourceName: "MissingOnServer")
        
        updateRow(at: indexPath, with: image)
    }
    
    func updateRow(at indexPath: IndexPath, with image: UIImage) {
        guard var row = rowAt(indexPath), row.image == nil else {
            return
        }
        
        row.image = image
        sections[indexPath.section].rows[indexPath.row] = row
        
        // Since this is also async, we need to make sure it doesn't display something it shouldn't
        delegate?.searchPhotoListVM(self, didUpdateRowsAt: [indexPath])
    }
    
    func derivedIndexPath(for pageInfo: PhotoPageInfo, photoPageIndex: Int) -> IndexPath {
        let rowNumber = (pageInfo.page - 1) * pageInfo.perPage + photoPageIndex
        let indexPath = IndexPath(row: rowNumber, section: 0)
        return indexPath
    }
    
    /**
     Finds the page that we need to load based what indexPath is shown,
     what we have already, and page count/size information
     */
    func pageNeeded(for indexPath: IndexPath, searchTerm: String) -> Int? {
        let pageForIndexPath = (Int(Double(indexPath.row / pageSize).rounded(.up))) + 1
        let hasPageForIndexPath = pages.contains(where: { $0.page == pageForIndexPath })
        
        // If we have the page for the given index path return nil
        guard !hasPageForIndexPath, pageForIndexPath > 0 else {
            return nil
        }
        
        guard let latestPage = pages.last else {
            let hasFirstPage = (pageRequestsInFlight[searchTerm]?.contains(1) ?? false)
            return hasFirstPage ? nil : 1
        }
        
        let hasRequestInFlightForPage = pageRequestsInFlight[searchTerm]?.contains(pageForIndexPath) ?? false
        guard !hasRequestInFlightForPage else {
            return nil
        }
        
        let isNextPageNeeded = pageForIndexPath <= latestPage.pages
        
        let page = isNextPageNeeded ? pageForIndexPath : nil
        return page
    }
}
