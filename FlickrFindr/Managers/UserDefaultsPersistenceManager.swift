//
//  UserDefaultsPersistenceManager.swift
//  FlickrFindr
//
//  Created by Michael Thomas on 4/1/18.
//  Copyright © 2018 Michael Thomas. All rights reserved.
//

import Foundation


class UserDefaultsPersistenceManager {
    private let defaults = UserDefaults.standard
    
    func saveValue<T>(_ value: T, forKey key: Key) {
        defaults.set(value, forKey: key.rawValue)
    }
    
    func object<T>(forKey key: Key) -> T? {
        return defaults.object(forKey: key.rawValue) as? T
    }
    
    @discardableResult func insertSetItem<T>(_ item: T, forKey key: Key) -> Set<T>? {
        // UserDefaults doesn't support Set's so we have to convert to/from
        let existingArray: [T] = object(forKey: key) ?? []
        
        var set = Set(existingArray)
        set.insert(item)
        
        saveValue(Array(set), forKey: key)
        let newArray: [T] = object(forKey: key) ?? []
        return Set(newArray)
    }

    enum Key: String {
        case searchHistory
        case test
    }
}
