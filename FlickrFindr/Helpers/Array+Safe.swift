//
//  Array+Safe.swift
//  FlickrFindr
//
//  Created by Michael Thomas on 4/2/18.
//  Copyright © 2018 Michael Thomas. All rights reserved.
//

import Foundation


extension Array {
    subscript (safe index: UInt) -> Element? {
        return self[safe: Int(index)]
    }
    
    subscript (safe index: Int) -> Element? {
        return index < count ? self[index] : nil
    }
}
