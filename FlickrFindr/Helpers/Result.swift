//
//  Result.swift
//  FlickrFindr
//
//  Created by Michael Thomas on 3/31/18.
//  Copyright © 2018 Michael Thomas. All rights reserved.
//

import Foundation


typealias ResultCompletion<T> = ((Result<T>) -> Void)

enum Result<T> {
    case success(T)
    case failure(Error)
}
