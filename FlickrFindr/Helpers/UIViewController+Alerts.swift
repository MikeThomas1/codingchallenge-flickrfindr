//
//  UIViewController+Alerts.swift
//  FlickrFindr
//
//  Created by Michael Thomas on 4/1/18.
//  Copyright © 2018 Michael Thomas. All rights reserved.
//

import UIKit


extension UIViewController {
    func presentError(message: String, retryHandler: (() -> Void)? = nil, completion: (() -> Void)? = nil) {
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        alertController.addAction(cancelAction)
        if let retryHandler = retryHandler {
            let retryAction = UIAlertAction(title: "Retry", style: .default) { (action) in
                retryHandler()
            }
            alertController.addAction(retryAction)
        }
        present(alertController, animated: true, completion: completion)
    }
}
