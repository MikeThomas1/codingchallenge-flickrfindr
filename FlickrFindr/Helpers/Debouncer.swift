//
//  Debouncer.swift
//  FlickrFindr
//
//  Created by Michael Thomas on 4/1/18.
//  Copyright © 2018 Michael Thomas. All rights reserved.
//

import Foundation


class Debouncer {
    var callback: (() -> ())
    var delay: Double
    weak var timer: Timer?
    
    init(delay: Double, callback: @escaping (() -> ())) {
        self.delay = delay
        self.callback = callback
    }
    
    func call() {
        timer?.invalidate()
//        let nextTimer = Timer.scheduledTimer(timeInterval: delay, target: self, selector: #selector(Debouncer.fireNow), userInfo: nil, repeats: false)
        let nextTimer = Timer.scheduledTimer(withTimeInterval: delay, repeats: false) { (timer) in
            self.callback()
        }
        timer = nextTimer
    }
}
