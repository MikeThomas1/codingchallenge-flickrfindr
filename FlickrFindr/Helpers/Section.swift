//
//  Section.swift
//  FlickrFindr
//
//  Created by Michael Thomas on 3/31/18.
//  Copyright © 2018 Michael Thomas. All rights reserved.
//

import Foundation


struct Section<SectionModel, Row> {
    var sectionInfo: SectionModel
    var rows: [Row]
}
