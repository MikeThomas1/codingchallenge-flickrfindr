//
//  UserDefaultsPersistenceManagerTests.swift
//  FlickrFindrTests
//
//  Created by Michael Thomas on 4/1/18.
//  Copyright © 2018 Michael Thomas. All rights reserved.
//

import XCTest
@testable import FlickrFindr


class UserDefaultsPersistenceManagerTests: XCTestCase {
    var manager: UserDefaultsPersistenceManager!
    override func setUp() {
        super.setUp()
        manager = UserDefaultsPersistenceManager()
    }
    
    override func tearDown() {
        manager = nil
        super.tearDown()
    }
    
    func testInsertingOneUniqueItem() {
        guard let set = manager.insertSetItem("test1", forKey: .test) else {
            XCTFail("insertSetItem returned nil")
            return
        }
        
        XCTAssertEqual(set.count, 1)
        XCTAssertTrue(set.contains("test1"))
    }
    
    func testInsertingTwoUniqueItems() {
        manager.insertSetItem("test1", forKey: .test)
        guard let set = manager.insertSetItem("test2", forKey: .test) else {
            XCTFail("insertSetItem returned nil")
            return
        }
        
        XCTAssertEqual(set.count, 2)
        XCTAssertTrue(set.contains("test1"))
        XCTAssertTrue(set.contains("test2"))
    }
    
    func testInsertingTwoIdenticalItems() {
        manager.insertSetItem("test1", forKey: .test)
        guard let set = manager.insertSetItem("test1", forKey: .test) else {
            XCTFail("insertSetItem returned nil")
            return
        }
        
        XCTAssertEqual(set.count, 1)
        XCTAssertTrue(set.contains("test1"))
    }
}
