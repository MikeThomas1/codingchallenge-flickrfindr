//
//  PhotoDetailsViewModelTests.swift
//  FlickrFindrTests
//
//  Created by Michael Thomas on 4/1/18.
//  Copyright © 2018 Michael Thomas. All rights reserved.
//

import XCTest
@testable import FlickrFindr


class PhotoDetailsViewModelTests: XCTestCase {
    var vm: PhotoDetailsViewModel!
    
    override func tearDown() {
        vm = nil
        super.tearDown()
    }

    func testSuccessfulImageFetch() {
        let expectation = XCTestExpectation(description: "fetchImage")
        let title = UUID().uuidString
        let photo = Photo(id: UUID().uuidString, owner: UUID().uuidString, secret: UUID().uuidString, server: UUID().uuidString, farm: 1, title: title, isPublic: 1, isFriend: 1, isFamily: 1)
        let client: FlickrClient = MockFlickrClient(.success)
        vm = PhotoDetailsViewModel(client, photo: photo)
        vm.fetchImage { (error) in
            XCTAssertNil(error)
            XCTAssertEqual(self.vm.details.title, title)
            expectation.fulfill()
        }
    }
    
    func testFailedImageFetch() {
        let expectation = XCTestExpectation(description: "fetchImage")
        let title = UUID().uuidString
        let photo = Photo(id: UUID().uuidString, owner: UUID().uuidString, secret: UUID().uuidString, server: UUID().uuidString, farm: 1, title: title, isPublic: 1, isFriend: 1, isFamily: 1)
        let client: FlickrClient = MockFlickrClient(.failure)
        vm = PhotoDetailsViewModel(client, photo: photo)
        vm.fetchImage { (error) in
            XCTAssertNotNil(error)
            XCTAssertEqual(self.vm.details.title, title)
            expectation.fulfill()
        }
    }
}
