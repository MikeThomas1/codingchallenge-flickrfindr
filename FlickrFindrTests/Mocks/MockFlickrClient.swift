//
//  MockFlickrClient.swift
//  FlickrFindrTests
//
//  Created by Michael Thomas on 4/1/18.
//  Copyright © 2018 Michael Thomas. All rights reserved.
//

import Foundation


class MockFlickrClient: FlickrClient {
    private let response: Response
    
    init(_ response: Response) {
        self.response = response
    }
    
    func fetchPhoto(_ photo: Photo, quality: PhotoQuality?, completion: @escaping ((Result<Data>) -> Void)) {
        switch response {
        case .success:
            completion(.success(Data()))
        case .failure:
            completion(.failure(NSError(domain: "flickr.tests", code: -1001)))
        }
    }
    
    func fetchPhotos(matching searchTerm: String, page: Int, perPage: Int, completion: @escaping ((Result<PhotosContainer>) -> Void)) {
        switch response {
        case .success:
            var info: PhotoPageInfo!
            for page in 1...25 {
                var photos = [Photo]()
                for _ in 1...25 {
                    let farm = Int(arc4random_uniform(100))
                    let isPublic = Int(arc4random_uniform(2))
                    let isFriend = Int(arc4random_uniform(2))
                    let isFamily = Int(arc4random_uniform(2))
                    let photo = Photo(id: UUID().uuidString, owner: UUID().uuidString, secret: UUID().uuidString, server: UUID().uuidString, farm: farm, title: UUID().uuidString, isPublic: isPublic, isFriend: isFriend, isFamily: isFamily)
                    photos.append(photo)
                }
                info = PhotoPageInfo(page: page, pages: 25, perPage: 25, total: "\(25*25)", photos: photos)
            }
            
            let photoContainer = PhotosContainer(pageInfo: info, stat: "ok")
            completion(.success(photoContainer))
        case .failure:
            completion(.failure(NSError()))
        }
    }
    
    enum Response {
        case success
        case failure
    }
}
